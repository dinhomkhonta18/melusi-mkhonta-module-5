// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:livestock_trade_app/auth/main_page.dart';
import 'pages/login_page.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: FirebaseOptions(
      apiKey: "AIzaSyD-_p4kVy_WdnxBwURDh1l9vlF_pJSNQQw",
      appId: "1:827381558512:android:255edb41762367425e2635",
      messagingSenderId: "125566710212",
      projectId: "new-livestock-trade",
    ),
  );

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: AnimatedSplashScreen(
        splash: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: Container(
                  height: 120,
                  width: 120,
                  decoration: BoxDecoration(
                    color: Colors.green,
                    image: DecorationImage(
                      image: NetworkImage('assets/images/farmers.png'),
                      fit: BoxFit.fill,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    border: Border.all(
                      color: Colors.black,
                      width: 2.0,
                    ),
                  ),
                ),
              ),
              Text(
                "LIVESTOCK TRADE",
                style: GoogleFonts.bebasNeue(
                  fontSize: 20,
                ),
              ),
            ],
          ),
        ),
        duration: 3000,
        splashTransition: SplashTransition.slideTransition,
        backgroundColor: Colors.green,
        nextScreen: const MainPage(),
      ),
    );
  }
}
